import { getUid, Map, View } from 'ol'
import { Vector as VectorLayer } from 'ol/layer'
import { Vector as VectorSource } from 'ol/source'
import { DragAndDrop, Draw, Modify, Select, Snap } from 'ol/interaction'
import { GeoJSON } from 'ol/format'
import { apply as applyStyle } from 'ol-mapbox-style'
import polygonClipping from 'polygon-clipping'

const GEOJSON_OPTS = { dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857' }
const geoJSONFormat = new GeoJSON(GEOJSON_OPTS)
const { VITE_MAPTILER_KEY } = import.meta.env

export function createMap(target, options) {
  const defaultOpts = { center: [0, 0], zoom: 2, }

  const map = new Map({
    target,
    view: new View(Object.assign({}, defaultOpts, options))
  })
  applyStyle(map, 'https://api.maptiler.com/maps/basic/style.json?key=' + VITE_MAPTILER_KEY)

  const source = new VectorSource()
  const vectorLayer = new VectorLayer({ source, zIndex: 10 })
  map.addLayer(vectorLayer)

  const select = new Select({ layers: [vectorLayer] })
  const interactions = {
    modify: new Modify({ features: select.getFeatures() }),
    select,
    drawPolygon: new Draw({ source, type: 'Polygon' }),
    snap: new Snap({ source }),
    dragAndDrop: new DragAndDrop({ formatConstructors: [GeoJSON] })
  }

  Object.values(interactions).forEach(i => map.addInteraction(i))

  interactions.dragAndDrop.on('addfeatures', function (e) {
    e.features.forEach(f => f.setProperties({ 'fileName': e.file.name }))
    source.addFeatures(e.features)
    map.getView().fit(source.getExtent())
  })

  // initially in editing mode
  interactions.modify.setActive(true)
  interactions.drawPolygon.setActive(false)

  return { map, source, interactions }
}

export function cloneMapFeature(mapFeature, dataFeature) {
  const json = featureToGeoJSON(mapFeature)
  const [copy] = geoJSONToFeatures(json, dataFeature.properties)
  copy.setId(getUid(copy))
  return copy
}

export function getDataFeature(mapFeature) {
  let id = mapFeature.getId()
  if (!id) {
    id = getUid(mapFeature)
    mapFeature.setId(id)
  }
  const { geometry, ...properties } = mapFeature.getProperties()
  return { id, properties }
}

export function geoJSONToFeatures(json, props) {
  const features = geoJSONFormat.readFeaturesFromObject(json, GEOJSON_OPTS)
  if (props) features.forEach(f => f.setProperties(props))
  return features
}

export function featureToGeoJSON(mapFeature, dataFeature) {
  const output = geoJSONFormat.writeFeatureObject(mapFeature, { decimals: 7 })
  if (dataFeature) {
    const { fileName, ...other } = dataFeature.properties
    output.properties = other
  }
  return output
}

export function featuresToGeoJSON(mapFeatures, dataFeatures) {
  const output = geoJSONFormat.writeFeaturesObject(mapFeatures, { decimals: 7 })

  if (Array.isArray(dataFeatures) && Array.isArray(output.features)) {
    const dfMap = {}
    dataFeatures.forEach(f => { dfMap[f.id.toString()] = f })

    output.features.forEach(f => {
      if (dfMap[f.id]) {
        const { fileName, ...other } = dfMap[f.id].properties
        f.properties = other
      }
    })
  }
  return output
}

export function downloadJsonFile(json, fileName) {
  const blobParts = [JSON.stringify(json, null, 2)]
  const fileBlob = new Blob(blobParts, { type: 'application/json' })
  const a = document.createElement('a')
  a.download = fileName
  a.href = URL.createObjectURL(fileBlob)
  setTimeout(() => URL.revokeObjectURL(a.href), 3E4)
  setTimeout(() => a.click(), 0)
}

export function doFeatureOperation(opType, features) {
  const opFn = polygonClipping[opType]
  const inCoords = features.map(f => featureToGeoJSON(f).geometry.coordinates)
  const outCoords = opFn.apply(null, inCoords)
  if (outCoords) {
    const outGeoJson = {
      type: 'Feature',
      geometry: { 'type': 'MultiPolygon', 'coordinates': outCoords }
    }
    return geoJSONToFeatures(outGeoJson)[0]
  }
}

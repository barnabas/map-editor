import { createStore } from 'vuex'

export const store = createStore({
  state () {
    return {
      isDrawing: false,
      selectedIds: {},
      features: []
    }
  },
  getters: {
    hasFeatures: state => state.features.length > 0,
    selectionCount: state => Object.values(state.selectedIds).filter(v => !!v).length
  },
  mutations: {
    addFeature (state, feature) {
      state.features.push(Object.assign({}, feature))
      if (feature.id) state.selectedIds = { ...state.selectedIds, [feature.id]: false }
    },
    clearFeatures (state) {
      state.selectedIds = {}
      state.features = []
    },
    removeFeature (state, feature) {
      const idx = state.features.findIndex(f => f.id === feature.id)
      if (idx < 0) return false
      state.features.splice(idx, 1)
      return true
    },
    setIsDrawing (state, isDrawing) {
      state.isDrawing = isDrawing
    },
    setSelectedIds (state, ids) {
      const newIds = {}
      Object.keys(state.selectedIds).forEach(id => newIds[id] = false)
      if (Array.isArray(ids)) ids.forEach(id => newIds[id] = true)
      state.selectedIds = newIds
    },
    toggleSelectedId (state, id) {
      state.selectedIds[id] = !state.selectedIds[id]
    },
    updateFeature (state, feature) {
      const idx = state.features.findIndex(f => f.id === feature.id)
      if (idx < 0) return false
      state.features[idx] = Object.assign({}, feature)
      return true
    }
  }
})
